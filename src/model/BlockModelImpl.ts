import {BlockModel} from "./BlockModel";
import EventEmitter = require("eventemitter3");

const X_EVENT = "x";
const Y_EVENT = "y";
const DESTROY_EVENT = "destroy";

export class BlockModelImpl extends EventEmitter implements BlockModel {
    private color = 0

    constructor(color: number) {
        super();
        this.color = color;
    }

    private _x = 0

    get x(): number {
        return this._x;
    }

    set x(value: number) {
        this._x !== value && this.emit(X_EVENT, this._x = value);
    }

    private _y = 0

    get y(): number {
        return this._y;
    }

    set y(value: number) {
        this._y !== value && this.emit(Y_EVENT, this._y = value);
    }

    getColor(): number {
        return this.color;
    }

    getX(): number {
        return this._x;
    }

    getY(): number {
        return this._y;
    }

    destroy(scored: boolean) {
        this.emit(DESTROY_EVENT, scored);
    }

    onX(callback: (x: number) => void) {
        this.addListener(X_EVENT, callback);
    }

    onY(callback: (y: number) => void) {
        this.addListener(Y_EVENT, callback);
    }

    onDestroyed(callback: (x: boolean) => void) {
        this.addListener(DESTROY_EVENT, callback)
    }
}
