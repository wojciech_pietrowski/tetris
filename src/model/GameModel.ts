import {BlockModel} from "./BlockModel";

export interface GameModel {
    onBlockAdded(callback: (model: BlockModel) => void)

    onScore(callback: (score: number) => void)

    onGameOver(callback: () => void)

    getScore(): number;

    isGameOver(): boolean;
}
