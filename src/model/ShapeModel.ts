import {BlockModelImpl} from './BlockModelImpl';
import {ShapeType} from "../tetris/dataInterfaces/ShapeType";

export class ShapeModel {
    type: ShapeType;
    blocks: BlockModelImpl[] = [];
    color: number;

    constructor(color: number) {
        this.color = color;
    }
}
