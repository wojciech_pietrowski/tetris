import {BlockModelImpl} from './BlockModelImpl';
import {ShapeModel} from './ShapeModel';
import {GameModel} from './GameModel';
import {BlockModel} from "./BlockModel";
import EventEmitter = require("eventemitter3");

const BLOCK_ADDED_EVENT = "blockAdded";
const SCORE_EVENT = "score";
const GAME_OVER_EVENT = "gameOver";

export class GameModelImpl extends EventEmitter implements GameModel {
    grid: BlockModelImpl[][];
    time = 0;
    processedTime = 0;
    private activeShape: ShapeModel;
    private gameOver = false;
    private score = 0;

    setScore(value: number) {
        this.score !== value && this.emit(SCORE_EVENT, this.score = value);
    }

    getScore(): number {
        return this.score;
    }

    setGameOver() {
        this.emit(GAME_OVER_EVENT, this.gameOver = true);
    }

    isGameOver(): boolean {
        return this.gameOver;
    }

    getBlock(x: number, y: number): BlockModel {
        return this.grid[x][y];
    }

    getActiveShape(): ShapeModel {
        return this.activeShape;
    }

    setActiveShape(activeShape: ShapeModel) {
        this.activeShape = activeShape;
        if (this.activeShape)
            this.activeShape.blocks.forEach(b => this.emit(BLOCK_ADDED_EVENT, b));
    }

    onBlockAdded(callback: (model: BlockModel) => void) {
        this.addListener(BLOCK_ADDED_EVENT, callback)
    }

    onScore(callback: (model: number) => void) {
        this.addListener(SCORE_EVENT, callback);
    }

    onGameOver(callback: () => void) {
        this.addListener(GAME_OVER_EVENT, callback);
    }
}
