export interface BlockModel {
    onY(callback: (x: number) => void);

    onX(callback: (x: number) => void);

    onDestroyed(callback: (x: boolean) => void);

    getX(): number

    getY(): number

    getColor(): number
}
