import {View} from './view/View';
import {Main} from './main/Main';

export class Tetris {
    private view: View;
    private main: Main;

    constructor() {
        this.main = new Main();
        this.view = new View(this.main);
    }
}
