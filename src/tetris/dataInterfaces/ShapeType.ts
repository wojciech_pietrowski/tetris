export type ShapeType = {
    id: number,
    color: string,
    shape: boolean[][]
}
