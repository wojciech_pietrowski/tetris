import {ShapeType} from './ShapeType';

export type ConfigType = {
    score: number;
    speed: number
    width: number;
    height: number
    shapes: ShapeType[]
}
