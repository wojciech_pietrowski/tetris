import {HEIGHT, WIDTH} from '../ViewSettings';
import {MainInterface} from '../../main/MainInterface';
import {Container, Text} from "pixi.js";

export class LoadingView extends Container {
    private label: Text;
    private value: Text;
    private main: MainInterface;

    constructor(main: MainInterface) {
        super();
        this.main = main;

        this.label = this.addChild(new Text('Loading'));
        this.label.anchor.set(.5, 1);
        this.label.position.set(WIDTH / 2, HEIGHT / 2);

        this.value = this.addChild(new Text('0%'));
        this.value.anchor.set(.5, 0);
        this.value.position.set(WIDTH / 2, HEIGHT / 2);

        main.onLoadingProgress((progress) => this.value.text = '' + Math.floor(progress * 100) + '%');
        main.onLoadingError(() => this.label.text = 'Loading Error');
        main.onStateChange(() => this.destroy(), true);
    }
}
