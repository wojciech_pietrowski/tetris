import {Graphics, Sprite} from "pixi.js";
import {BLOCK_SIZE} from "../ViewSettings";
import {Assets} from "../Assets";

export class BackgroundBlock extends Graphics {
    constructor(color: number) {
        super();
        this.beginFill(color).drawRect(0, 0, BLOCK_SIZE, BLOCK_SIZE);
        this.addChild(Sprite.from(Assets.block));
    }
}
