import {ConfigType} from '../../dataInterfaces/ConfigType';
import {BackgroundShape} from './BackgroundShape';
import {WIDTH} from '../ViewSettings';
import {MainInterface} from '../../main/MainInterface';

import {Container, Sprite, Text} from 'pixi.js'
import {Assets} from "../Assets";

export class Background extends Container {
    private title: Text;
    private shapes: BackgroundShape[];

    constructor(main: MainInterface) {
        super();
        this.addChild(Sprite.from(Assets.background));
        this.title = this.addChild(new Text('Tetris', {fill: 0xffffff}));
        this.title.anchor.set(.5, 0);
        this.title.position.set(WIDTH * .75, 50);

        if (main.getConfig) {
            this.processConfig(main.getConfig());
        } else {
            main.onStateChange(() => this.processConfig(main.getConfig()), true);
        }
    }

    private processConfig(config: ConfigType) {
        this.shapes = config.shapes.map((shapeConfig, index) => {
            const shape = new BackgroundShape(shapeConfig);
            shape.x = (index + .5) * WIDTH / 2 / config.shapes.length;
            shape.y = 50;
            shape.scale.set(.25);
            return this.addChild(shape);
        });
    }
}
