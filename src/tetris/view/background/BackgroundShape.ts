import {Container, Graphics, Ticker} from 'pixi.js'
import {BLOCK_SIZE} from '../ViewSettings';
import {ShapeType} from '../../dataInterfaces/ShapeType';
import {BackgroundBlock} from "./BackgroundBlock";

export class BackgroundShape extends Container {
    private shape: Graphics;
    private dRotation: number;

    constructor(shapeConfig: ShapeType) {
        super();
        this.dRotation = Math.random() * .02 + .001;
        const color = parseInt('0x' + shapeConfig.color.substr(1));
        this.shape = this.addChild(new Graphics()).beginFill(color);
        let height = 0;
        let width = 0;

        shapeConfig.shape.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value) {
                    let block = this.shape.addChild(new BackgroundBlock(color))
                    block.x = x * BLOCK_SIZE;
                    block.y = y * BLOCK_SIZE;
                    width = Math.max(width, x + 1);
                    height = Math.max(height, y + 1);
                }
            })
        })
        this.shape.x -= width / 2 * BLOCK_SIZE;
        this.shape.y -= height / 2 * BLOCK_SIZE;


        Ticker.shared.add(this.rotate, this)
    }

    private rotate() {
        this.rotation += this.dRotation;
    }
}
