import {HEIGHT, WIDTH} from './ViewSettings';
import {Application, Container} from "pixi.js";

export class ViewCanvas {
    private rootContainer: HTMLDivElement;
    private pixiApplication: Application;

    constructor() {
        this.rootContainer = this.createMainContainer();
        this.pixiApplication = new Application({
            width: WIDTH,
            height: HEIGHT,
            backgroundColor: 0xffffff,
        });
        this.rootContainer.appendChild(this.pixiApplication.view);
        this.autosize();
    }

    get stage(): Container {
        return this.pixiApplication.stage;
    }

    private createMainContainer() {
        const div = document.createElement("div")
        div.style.position = "absolute";
        div.style.width = "90%";
        div.style.height = "90%";
        div.style.left = "5%";
        div.style.top = "5%";
        div.style.border = "solid darkred";
        document.body.append(div);
        return div
    }

    private autosize(): void {
        let availableWidth = 0;
        let availableHeight = 0;

        const updateSize = () => {
            availableWidth = this.rootContainer.clientWidth;
            availableHeight = this.rootContainer.clientHeight;

            let scale = availableWidth / WIDTH;

            if (scale * HEIGHT > availableHeight) {
                scale = availableHeight / HEIGHT;
            }

            this.pixiApplication.view.style.transform = 'scale(' + scale + ') translate(-50%,0)';
            this.pixiApplication.view.style.transformOrigin = 'left top';
            this.pixiApplication.view.style.position = 'absolute';
            this.pixiApplication.view.style.left = '50%';
        }

        window.addEventListener("resize", () => updateSize());
        new MutationObserver(() => updateSize());
        updateSize();
    }
}
