import {MenuButton} from './MenuButton';
import {BLOCK_SIZE, BOARD_LEFT, BOARD_TOP} from '../ViewSettings';
import {MainInterface} from '../../main/MainInterface';
import {Container} from "pixi.js";


export class MenuView extends Container {

    constructor(main: MainInterface) {
        super();
        const button = this.addChild(new MenuButton("GRAJ", () => main.newGame()));
        button.x = BOARD_LEFT + main.getConfig().width / 2 * BLOCK_SIZE;
        button.y = BOARD_TOP + main.getConfig().height / 2 * BLOCK_SIZE;
        main.onStateChange(() => this.destroy(), true);
    }

}
