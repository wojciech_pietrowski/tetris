import {Graphics, Sprite, Text} from 'pixi.js'

export class MenuButton extends Sprite {
    static HEIGHT = 40;
    static WIDTH = 200;
    private background: Graphics;
    private label: Text;

    constructor(label: string, onClick: () => void) {
        super();
        this.background = this.addChild(new Graphics()).beginFill(0x9999ff).drawRect(-MenuButton.WIDTH / 2, -MenuButton.HEIGHT / 2, MenuButton.WIDTH, MenuButton.HEIGHT);
        this.label = this.background.addChild(new Text(label));
        this.label.anchor.set(.5);
        this.interactive = true;


        this.addListener('pointertap', onClick);
        this.addListener('pointerover', () => this.scale.set(1.1));
        this.addListener('pointerout', () => this.scale.set(1));
        this.addListener('pointeroutoutside', () => this.scale.set(1));
    }
}
