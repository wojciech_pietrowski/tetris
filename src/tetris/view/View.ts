import {ViewCanvas} from './ViewCanvas';
import {LoadingView} from './loading/LoadingView';
import {Background} from './background/Background';
import {MenuView} from './menu/MenuView';
import {GameView} from './game/GameView';
import {MainState} from '../main/MainState';
import {MainInterface} from '../main/MainInterface';

import {Assets} from "./Assets";
import {ViewLoader} from "./ViewLoader";
import {Loader} from "pixi.js";

export class View {

    private main: MainInterface;
    private background: Background;
    private canvas: ViewCanvas;

    constructor(main: MainInterface) {

        this.main = main;
        this.canvas = new ViewCanvas();

        this.load().then(() => {
            this.background = this.canvas.stage.addChild(new Background(main));
            this.main.onStateChange((state) => this.onState(state), false);
            this.onState(this.main.getState());
        })
    }

    private load() {
        const loader = Loader.shared;
        this.canvas.stage.addChild(new ViewLoader(loader));
        Object.values(Assets).forEach(url => loader.add(url, url));
        return new Promise<View>(resolve => {
            loader.onComplete.add(() => resolve(this));
            loader.load();
        });
    }

    private onState(state: MainState) {
        switch (state) {
            case MainState.opening:
                break;
            case MainState.loading:
                return this.canvas.stage.addChild(new LoadingView(this.main));
            case MainState.menu:
                return this.canvas.stage.addChild(new MenuView(this.main));
            case MainState.game:
                return this.canvas.stage.addChild(new GameView(this.main));
            default:
                throw new Error('State not implemented ' + state);
        }
    }
}
