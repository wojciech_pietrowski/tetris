export const WIDTH = 640;
export const HEIGHT = 960;

export const BLOCK_SIZE = 42;

export const LOADER_BAR_BAR_WIDTH = 200;
export const LOADER_BAR_THICK = 20;
export const LOADER_BAR_FOREGROUND = 0x000000;
export const LOADER_BAR_BACKGROUND = 0xffffff;

export const BOARD_LEFT = 8;
export const BOARD_TOP = 110;

export const BACKGROUND_TILE_FILL = 0x434343;
export const BACKGROUND_TILE_STROKE = 0x373737;
export const BACKGROND_TILE_LINE_WIDTH = 4;
