import {
    HEIGHT,
    LOADER_BAR_BACKGROUND,
    LOADER_BAR_BAR_WIDTH,
    LOADER_BAR_FOREGROUND,
    LOADER_BAR_THICK,
    WIDTH
} from "./ViewSettings";
import {Container, Graphics, Loader} from "pixi.js";

export class ViewLoader extends Container {

    progressBarForeground: Graphics;
    progressBarBackground: Graphics;
    private progressBar: Container;

    constructor(loader: Loader) {
        super();
        Loader.shared.onProgress.add(loader => this.update(loader.progress / 100));
        Loader.shared.onComplete.add(() => this.destroy());

        this.progressBar = this.addChild(new Container());
        this.progressBar.x = WIDTH / 2;
        this.progressBar.y = HEIGHT / 2;

        this.progressBarBackground = this.progressBar.addChild(new Graphics());
        this.progressBarBackground.beginFill(LOADER_BAR_BACKGROUND).drawRoundedRect(-LOADER_BAR_BAR_WIDTH / 2, -LOADER_BAR_THICK / 2, LOADER_BAR_BAR_WIDTH, LOADER_BAR_THICK, 10);
        this.progressBarForeground = this.progressBar.addChild(new Graphics());

        this.update(0);
    }

    update(progress: number): void {
        const left = -(LOADER_BAR_BAR_WIDTH - 10) / 2;
        const top = -LOADER_BAR_THICK / 4;
        const width = 10 + (LOADER_BAR_BAR_WIDTH - 20) * progress;
        const height = LOADER_BAR_THICK / 2;
        this.progressBarForeground.clear().beginFill(LOADER_BAR_FOREGROUND).drawRoundedRect(left, top, width, height, 5);
    }
}
