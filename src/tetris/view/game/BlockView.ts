import {Graphics, Sprite, Ticker} from "pixi.js";
import {BlockModel} from "../../../model/BlockModel";
import {BLOCK_SIZE} from "../ViewSettings";
import {Assets} from "../Assets";
import {ConfigType} from "../../dataInterfaces/ConfigType";

export class BlockView extends Graphics {
    constructor(config: ConfigType, block: BlockModel) {
        super();
        this.beginFill(block.getColor()).drawRect(0, 0, BLOCK_SIZE, BLOCK_SIZE);
        this.addChild(Sprite.from(Assets.block));
        block.onX(x => this.onX(x));
        block.onY(y => this.onY(y));
        block.onDestroyed((scored) => {
            if (scored) {
                let time = 0;
                const update = () => {
                    time += Ticker.shared.deltaMS;
                    const p = Math.min(1, time / config.speed);
                    this.alpha = 1 - p;
                    if (p == 1) {
                        this.destroy();
                        Ticker.shared.remove(update);
                    }
                }
                Ticker.shared.add(update);
            } else {
                this.destroy();
            }
        });
        this.onX(block.getX());
        this.onY(block.getY());
    }

    private onX(x: number) {
        this.x = x * BLOCK_SIZE
    }

    private onY(y: number) {
        this.y = y * BLOCK_SIZE
    }
}
