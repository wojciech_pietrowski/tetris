import {Graphics, Sprite} from "pixi.js";
import {GameModel} from "../../../model/GameModel";
import {BlockView} from "./BlockView";
import {ConfigType} from "../../dataInterfaces/ConfigType";
import {BACKGROND_TILE_LINE_WIDTH, BACKGROUND_TILE_FILL, BACKGROUND_TILE_STROKE, BLOCK_SIZE} from "../ViewSettings";
import {Assets} from "../Assets";


export class Blocks extends Graphics {
    private model: GameModel;

    constructor(config: ConfigType, model: GameModel) {
        super();
        this.model = model;

        this.beginFill(BACKGROUND_TILE_FILL).lineStyle(BACKGROND_TILE_LINE_WIDTH, BACKGROUND_TILE_STROKE);
        for (let x = 0; x < config.width; x++)
            for (let y = 0; y < config.height; y++)
                this.drawRect(x * BLOCK_SIZE, y * BLOCK_SIZE, BLOCK_SIZE, BLOCK_SIZE);

        const sprite = this.addChild(Sprite.from(Assets.gradient))
        sprite.width = config.width * BLOCK_SIZE;
        sprite.height = config.height * BLOCK_SIZE;

        this.model.onBlockAdded(model => this.addChild(new BlockView(config, model)));
    }
}
