import {Blocks} from './Blocks';
import {BLOCK_SIZE, BOARD_LEFT, BOARD_TOP} from '../ViewSettings';
import {MenuButton} from '../menu/MenuButton';
import {MainInterface} from '../../main/MainInterface';
import {Container, Text, Ticker} from "pixi.js";
import {GameModel} from "../../../model/GameModel";
import {GameController} from "../../main/gameController/GameController";


export class GameView extends Container {
    private main: MainInterface;
    private board: Blocks;
    private scoreLabel: Text;
    private scoreValue: Text;
    private back: MenuButton;
    private onKeyDown: (e: KeyboardEvent) => void;
    private onKeyUp: (e: KeyboardEvent) => void;
    private pressedKeys: Record<string, boolean> = {};
    private model: GameModel;
    private gameController: GameController;

    constructor(main: MainInterface) {
        super();
        this.main = main;
        this.gameController = main.getGameController();
        this.model = this.gameController.getModel();

        this.board = this.addChild(new Blocks(main.getConfig(), this.model));
        this.board.x = BOARD_LEFT;
        this.board.y = BOARD_TOP;

        this.scoreLabel = this.addChild(new Text('Score:', {fill: 0xffffff}));
        this.scoreLabel.anchor.set(.5, 1);
        this.scoreValue = this.addChild(new Text('000000', {fill: 0xffffff}));
        this.scoreValue.anchor.set(.5, 0);
        this.scoreLabel.x = this.scoreValue.x = 540;
        this.scoreLabel.y = this.scoreValue.y = 780;

        this.back = this.addChild(new MenuButton('WRÓC', () => this.main.showMenu()));
        this.back.x = BOARD_LEFT + main.getConfig().width / 2 * BLOCK_SIZE;
        this.back.y = BOARD_TOP + main.getConfig().height / 2 * BLOCK_SIZE;
        this.back.visible = false;

        Ticker.shared.add(this.update, this);


        this.model.onGameOver(() => this.onGameOver());
        this.model.onScore(score => this.onScore(score));
        this.onScore(this.model.getScore());

        this.main.onStateChange(() => this.destroy(), true);

        this.onKeyDown = (e: KeyboardEvent) => {
            if (this.pressedKeys[e.key]) return;
            this.pressedKeys[e.key] = true;

            if (e.key === "ArrowLeft") this.gameController.moveLeft();
            if (e.key === "ArrowRight") this.gameController.moveRight();
            if (e.key === "ArrowDown") this.gameController.moveDown();
            if (e.key === "ArrowUp") this.gameController.rotate();
        }
        this.onKeyUp = (e: KeyboardEvent) => {
            this.pressedKeys[e.key] = false;
        }
        window.addEventListener("keydown", this.onKeyDown);
        window.addEventListener("keyup", this.onKeyUp);
    }

    destroy(options?: { children?: boolean; texture?: boolean; baseTexture?: boolean }): void {
        Ticker.shared.remove(this.update, this);
        return super.destroy(options);
    }

    update(): void {
        this.gameController.update(Ticker.shared.deltaMS);
    }

    private onGameOver() {
        Ticker.shared.remove(this.update, this);
        window.removeEventListener("keydown", this.onKeyDown);
        window.removeEventListener("keyup", this.onKeyUp);
        this.back.visible = true;
    }

    private onScore(score: number) {
        this.scoreValue.text = ('000000' + score).substr(-6);
    }
}
