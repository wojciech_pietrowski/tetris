export const Assets: Record<string, string> = {
    background: "assets/bg_full.jpg",
    block: "assets/block.png",
    gradient: "assets/gradient.png"
}
