import {ConfigType} from "../../dataInterfaces/ConfigType";
import EventEmitter = require("eventemitter3");

const ERROR_EVENT = "error";
const LOAD_EVENT = "load";
const PROGRESS_EVENT = "progress";

export class ConfigLoader extends EventEmitter {
    private request: XMLHttpRequest;
    private url: string;
    private loadedBytes = 0;
    private totalBytes = 1;

    constructor(url: string) {
        super();
        this.url = url;
    }

    get loadingProgress(): number {
        return this.totalBytes ? this.loadedBytes / this.totalBytes : 0;
    }

    load(): void {
        if (!this.request) {

            const saveResult = (result: ConfigType) => this.emit(LOAD_EVENT, result);
            const saveError = (error: string) => this.emit(ERROR_EVENT, error);

            this.request = new XMLHttpRequest();
            this.request.addEventListener('abort', () => saveError('aborted'));
            this.request.addEventListener('error', () => saveError('error'));
            this.request.addEventListener('progress', (e) => {
                this.loadedBytes = e.loaded;
                this.totalBytes = e.total;
                this.emit(PROGRESS_EVENT, this.loadingProgress);
            });

            this.request.addEventListener('load', () => {
                let responseObject;
                try {
                    responseObject = JSON.parse(this.request.responseText);
                } catch (e) {
                    return saveError('parseError');
                }
                saveResult(responseObject);
            });

            this.request.open('GET', this.url, true);
            this.request.send();
        }
    }

    onError(callback: (message) => void) {
        this.addListener(ERROR_EVENT, callback);
    }

    onLoad(callback: (message: ConfigType) => void) {
        this.addListener(LOAD_EVENT, callback);
    }

    onProgress(callback: (progress: number) => void) {
        this.addListener(PROGRESS_EVENT, callback);
    }
}
