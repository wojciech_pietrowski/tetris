import {MainState} from './MainState';
import {ConfigType} from '../dataInterfaces/ConfigType';
import {GameController} from "./gameController/GameController";

export interface MainInterface {

    getState(): MainState;

    getConfig(): ConfigType;

    onStateChange(callback: (state: MainState) => void, once: boolean): void;

    onLoadingError(callback: (error: string) => void);

    onLoadingProgress(callback: (progress: number) => void);

    newGame(): void;

    showMenu(): void;

    getGameController(): GameController
}

