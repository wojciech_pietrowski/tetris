import {ConfigType} from '../dataInterfaces/ConfigType';

import {ConfigLoader} from './loading/ConfigLoader';
import {CONFIG_URL} from './MainSettings';
import {GameModelImpl} from '../../model/GameModelImpl';
import {MainState} from './MainState';
import {MainInterface} from './MainInterface';
import {GameController} from "./gameController/GameController";
import {GameControllerImpl} from "./gameController/GameControllerImpl";
import EventEmitter = require("eventemitter3");

const STATE_EVENT = "state";
const PROGRESS_EVENT = "progress";
const ERROR_EVENT = "error";

export class Main extends EventEmitter implements MainInterface {

    private state: MainState = MainState.opening;
    private config: ConfigType;
    private error: string;
    private configLoader: ConfigLoader;
    private gameController: GameController;

    constructor() {
        super();
        this.load();
    }

    onStateChange(callback: (state: MainState) => void, once = false) {
        once ? this.once(STATE_EVENT, callback) : this.on(STATE_EVENT, callback);
    }

    onLoadingError(callback: (error: string) => void) {
        this.on(ERROR_EVENT, callback);
    }

    onLoadingProgress(callback: (progress: number) => void) {
        this.on(PROGRESS_EVENT, callback);
    }

    getState(): MainState {
        return this.state;
    }

    getConfig(): ConfigType {
        return this.config;
    }

    showMenu(): void {
        this.setState(MainState.menu);
    }

    newGame(): void {
        const gameControllerImpl = new GameControllerImpl(new GameModelImpl(), this.config);
        gameControllerImpl.init();
        this.gameController = gameControllerImpl
        this.setState(MainState.game);
    }

    getGameController(): GameController {
        return this.gameController;
    }

    private load() {
        this.setState(MainState.loading);
        this.configLoader = new ConfigLoader(CONFIG_URL);
        this.configLoader.onProgress(progress => this.emit(PROGRESS_EVENT, progress));
        this.configLoader.onError((error) => (this.error = error) && this.emit(ERROR_EVENT, error));
        this.configLoader.onLoad(response => (this.config = response) && this.showMenu());
        this.configLoader.load();
    }

    private setState(value: MainState) {
        this.state !== value && this.emit(STATE_EVENT, this.state = value);
    }
}

