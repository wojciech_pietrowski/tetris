import {GameControllerImpl} from "./GameControllerImpl";
import {ShapeModel} from "../../../model/ShapeModel";
import {BlockModelImpl} from "../../../model/BlockModelImpl";


function toGrid(pattern: number[][]): BlockModelImpl[][] {
    const grid = [];
    pattern.forEach((row, y) => row.forEach((value, x) => {
        !grid[x] && (grid[x] = []);
        if (value) {
            const blockModelImpl = new BlockModelImpl(0);
            blockModelImpl.x = x;
            blockModelImpl.y = y;
            grid[x][y] = blockModelImpl;
        }
    }));
    return grid;
}

test('getEmptyLinesShould return only lines possible to clear', () => {
    const config: any = {
        width: 2,
        height: 4
    }
    const activeShape = new ShapeModel(0)

    const gameModel: any = {
        activeShape,
        grid: toGrid([
            [0, 0],
            [1, 0],
            [0, 0],
            [1, 0]
        ])
    };
    const gameControllerImpl = new GameControllerImpl(gameModel, config);
    const lines = gameControllerImpl.getEmptyLines()
    expect(lines).toStrictEqual([2]);
});

test('getEmptyLinesShould should return empyt array if no blocks', () => {
    const config: any = {
        width: 2,
        height: 4
    }
    const activeShape = new ShapeModel(0)

    const gameModel: any = {
        activeShape,
        grid: toGrid([
            [0, 0],
            [0, 0],
            [0, 0],
            [0, 0]
        ])
    };
    const gameControllerImpl = new GameControllerImpl(gameModel, config);
    const lines = gameControllerImpl.getEmptyLines()
    expect(lines).toStrictEqual([]);
});
