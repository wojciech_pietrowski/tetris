import {GameControllerImpl} from "./GameControllerImpl";
import {ShapeModel} from "../../../model/ShapeModel";
import {BlockModelImpl} from "../../../model/BlockModelImpl";

test('shape should be not updated when cant move right', () => {
    const config: any = {
        width: 3
    }
    const activeShape = new ShapeModel(0)

    const firstBlock = new BlockModelImpl(0);
    firstBlock.x = 1;
    activeShape.blocks.push(firstBlock);

    const secondBlock = new BlockModelImpl(0);
    secondBlock.x = 2;
    activeShape.blocks.push(secondBlock);

    const gameModel: any = {
        activeShape,
        getBlock: (x, y) => null,
    };
    new GameControllerImpl(gameModel, config).moveRight()

    expect(firstBlock.x).toBe(1);
    expect(secondBlock.x).toBe(2);
});

test('shape should be not updated when new field occupied', () => {
    const config: any = {
        width: 3
    }
    const activeShape = new ShapeModel(0)

    const firstBlock = new BlockModelImpl(0);
    firstBlock.x = 0;
    activeShape.blocks.push(firstBlock);

    const secondBlock = new BlockModelImpl(0);
    secondBlock.x = 1;
    activeShape.blocks.push(secondBlock);

    const gameModel: any = {
        activeShape,
        getBlock: (x, y) => new BlockModelImpl(0),
    };
    new GameControllerImpl(gameModel, config).moveRight()

    expect(firstBlock.x).toBe(0);
    expect(secondBlock.x).toBe(1);
});

test('shape should be updated when move right', () => {
    const config: any = {
        width: 3
    }

    const activeShape = new ShapeModel(0)

    const firstBlock = new BlockModelImpl(0);
    firstBlock.x = 0;
    activeShape.blocks.push(firstBlock);

    const secondBlock = new BlockModelImpl(0);
    secondBlock.x = 1;
    activeShape.blocks.push(secondBlock);

    const gameModel: any = {
        activeShape,
        getBlock: (x, y) => null,
    };

    new GameControllerImpl(gameModel, config).moveRight()

    expect(firstBlock.x).toBe(1);
    expect(secondBlock.x).toBe(2);
});
