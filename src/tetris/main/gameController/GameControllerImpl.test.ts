import {GameControllerImpl} from "./GameControllerImpl";
import {ShapeModel} from "../../../model/ShapeModel";
import {BlockModelImpl} from "../../../model/BlockModelImpl";

test('position of blocks should be updated after shape move', () => {
    const config: any = {}
    const gameModel: any = {};
    const shape = new ShapeModel(0)

    const firstBlock = new BlockModelImpl(0);
    firstBlock.x = 1;
    shape.blocks.push(firstBlock);

    const secondBlock = new BlockModelImpl(0);
    secondBlock.x = 2;
    shape.blocks.push(secondBlock);

    new GameControllerImpl(gameModel, config).moveShape(shape, 2)

    expect(firstBlock.x).toBe(3);
    expect(secondBlock.x).toBe(4);
});
