import {GameController} from "./GameController";
import {GameModelImpl} from "../../../model/GameModelImpl";
import {GameModel} from "../../../model/GameModel";
import {ShapeModel} from "../../../model/ShapeModel";
import {ConfigType} from "../../dataInterfaces/ConfigType";
import {BlockModelImpl} from "../../../model/BlockModelImpl";

export class GameControllerImpl implements GameController {
    private gameModel: GameModelImpl;
    private config: ConfigType;

    constructor(gameModel: GameModelImpl, config: ConfigType) {
        this.gameModel = gameModel;
        this.config = config;
    }

    init() {
        this.gameModel.grid = [];
        for (let col = 0; col < this.config.width; col++) {
            this.gameModel.grid[col] = [];
            for (let row = 0; row < this.config.height; row++) {
                this.gameModel.grid[col][row] = null;
            }
        }
    }

    getModel(): GameModel {
        return this.gameModel;
    }

    moveRight() {
        if (!this.gameModel.getActiveShape()) return;
        for (const block of this.gameModel.getActiveShape().blocks) {
            const nextX = block.x + 1;
            if (nextX >= this.config.width) return;
            if (this.gameModel.getBlock(nextX, block.y)) return;
        }
        this.moveShape(this.gameModel.getActiveShape(), 1);
    }

    moveLeft() {
        if (!this.gameModel.getActiveShape()) return;
        for (const block of this.gameModel.getActiveShape().blocks) {
            const nextX = block.x - 1;
            if (nextX < 0) return;
            if (this.gameModel.getBlock(nextX, block.y)) return;
        }
        this.moveShape(this.gameModel.getActiveShape(), -1);
    }

    moveDown() {
        while (this.canMove())
            this.moveShapeDown(this.gameModel.getActiveShape());
        this.gameModel.processedTime = this.gameModel.time - this.config.speed;
    }

    rotate() {
        if (!this.gameModel.getActiveShape()) return;
        const newShape = this.rotateShape(this.gameModel.getActiveShape());

        if (this.isShapeFitGrid(newShape, this.gameModel.grid)) {
            this.gameModel.getActiveShape().blocks.forEach(b => b.destroy(false));
            this.gameModel.setActiveShape(newShape);

        } else {
            const max = this.gameModel.getActiveShape().blocks.reduce((max, b) => Math.max(b.x, max), 0);
            for (let offset = 1; offset <= max; offset++) {
                for (const direction of [-1, 1]) {
                    this.moveShape(newShape, offset * direction);
                    if (this.isShapeFitGrid(newShape, this.gameModel.grid)) {
                        this.gameModel.getActiveShape().blocks.forEach(b => b.destroy(false));
                        this.gameModel.setActiveShape(newShape);
                        return;
                    }
                    this.moveShape(newShape, -offset * direction);
                }
            }
        }
    }

    update(milliseconds: number): void {
        this.gameModel.time += milliseconds;
        while (!this.gameModel.isGameOver() && this.gameModel.processedTime + this.config.speed <= this.gameModel.time) {
            this.gameModel.processedTime += this.config.speed;
            this.drop();
        }
    }

    drop(): void {
        const emptyLines = this.getEmptyLines();
        if (emptyLines.length) {
            return this.removeEmptyLines(emptyLines);
        }

        const linesToClear = this.getLinesToClear();
        if (linesToClear.length) {
            return this.clearLines(linesToClear);
        }

        if (this.isOverflow()) {
            this.gameModel.setGameOver();
            return;
        }

        if (this.hasActiveShape()) {
            return this.move();
        }

        return this.generateShape();
    }

    getEmptyLines(): number[] {
        const emptyLines = [];
        let topBlockY = this.config.height;

        for (let y = 0; y < this.config.height; y++) {
            let isEmpty = true;
            for (let x = 0; x < this.config.width; x++) {
                if (this.gameModel.grid[x][y]) {
                    isEmpty = false;
                    topBlockY = Math.min(y, topBlockY);
                }
            }
            isEmpty && emptyLines.push(y);
        }
        return emptyLines.filter(y => y > topBlockY);
    }

    rotateShape(shapeModel: ShapeModel): ShapeModel {

        const blocks = [];
        let minX = Number.MAX_SAFE_INTEGER;
        let minY = Number.MAX_SAFE_INTEGER;
        let maxX = 0;
        let maxY = 0;
        shapeModel.blocks.forEach(b => {
            const newBlock = new BlockModelImpl(b.getColor());
            newBlock.x = b.x;
            newBlock.y = b.y;
            minX = Math.min(minX, b.x);
            minY = Math.min(minY, b.y);
            maxX = Math.max(maxX, b.x);
            maxY = Math.max(maxY, b.y);
            blocks.push(newBlock);
        });
        const width = maxX - minX;
        const height = maxY - minY;

        blocks.forEach(b => {
            b.x -= minX;
            b.y -= minY;
        })

        blocks.forEach(b => {
            const newX = height - b.y;
            const newY = b.x;
            b.x = newX + minX;
            b.y = newY + minY;
        });


        const newShape = new ShapeModel(shapeModel.color);
        newShape.blocks = blocks;
        return newShape;
    }

    moveShape(shapeModel: ShapeModel, amount = 1) {
        shapeModel.blocks.forEach(b => b.x += amount);
    }

    moveShapeDown(shapeModel: ShapeModel,) {
        shapeModel.blocks.forEach(b => b.y++);
    }

    isShapeFitGrid(shapeModel: ShapeModel, grid: BlockModelImpl[][]) {
        for (const block of shapeModel.blocks) {
            if (block.x < 0) return false;
            if (block.x >= grid.length) return false;
            if (block.y < 0) return false;
            if (block.y >= grid[0].length) return false;
            if (grid[block.x][block.y]) return false;
        }
        return true;
    }

    private removeEmptyLines(emptyLines: number[]) {
        for (let x = 0; x < this.config.width; x++) {
            emptyLines.sort().reverse().forEach(y => this.gameModel.grid[x].splice(y, 1));
            emptyLines.forEach(() => this.gameModel.grid[x].unshift(null));
        }
        this.gameModel.grid.forEach((row, x) => row.forEach((block, y) => block && (block.y = y)));
    }

    private getLinesToClear(): number[] {
        const lines = [];
        for (let y = 0; y < this.config.height; y++) {
            let canClean = true;
            for (let x = 0; x < this.config.width; x++) {
                if (!this.gameModel.getBlock(x, y)) canClean = false;
            }
            canClean && lines.push(y);
        }
        return lines;
    }

    private clearLines(linesToClear: number[]) {
        for (let x = 0; x < this.config.width; x++) {
            linesToClear.forEach(y => {
                this.gameModel.grid[x][y].destroy(true);
                this.gameModel.grid[x][y] = null;
            });
            this.gameModel.setScore(this.gameModel.getScore() + this.config.score);
        }
    }

    private isOverflow() {
        if (this.gameModel.getActiveShape())
            for (const block of this.gameModel.getActiveShape().blocks) {
                if (this.gameModel.getBlock(block.x, block.y))
                    return true;
            }
        return false;
    }

    private hasActiveShape() {
        return this.gameModel.getActiveShape() != null;
    }

    private move() {
        if (this.canMove()) {
            this.moveShapeDown(this.gameModel.getActiveShape());
        } else {
            this.splitActiveShape();
        }
    }

    private splitActiveShape() {
        this.gameModel.getActiveShape().blocks.forEach(b => this.gameModel.grid[b.x][b.y] = b);
        this.gameModel.setActiveShape(null);
    }

    private canMove() {
        if (!this.gameModel.getActiveShape()) return false;
        for (const b of this.gameModel.getActiveShape().blocks) {
            const nextX = b.x;
            const nextY = b.y + 1;
            if (nextY >= this.config.height || this.gameModel.getBlock(nextX, nextY)) {
                return false;
            }
        }
        return true;
    }

    private generateShape() {
        const randomShape = this.config.shapes[Math.floor(Math.random() * this.config.shapes.length)];
        const color = parseInt('0x' + randomShape.color.substr(1));

        let shape = new ShapeModel(color);
        randomShape.shape.forEach((row, y) => {
            row.forEach((value, x) => {
                if (value) {
                    const blockModel = new BlockModelImpl(color);
                    blockModel.x = x;
                    blockModel.y = y;
                    shape.blocks.push(blockModel)
                }
            });
        });
        const rotations = Math.random() * 4;
        for (let i = 0; i < rotations; i++)
            shape = this.rotateShape(shape);

        let maxX = 0;
        shape.blocks.forEach(b => maxX = Math.max(maxX, b.x))
        this.moveShape(shape, Math.floor((this.config.width - maxX) / 2))
        this.gameModel.setActiveShape(shape)
    }
}
