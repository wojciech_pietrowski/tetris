import {GameModel} from "../../../model/GameModel";

export interface GameController {
    getModel(): GameModel;

    moveLeft(): void;

    moveRight(): void;

    moveDown(): void;

    update(deltaMS: number): void;

    rotate(): void;
}
